// nomor 1

// import 'dart:io';

// void main(List<String> args) {
//   print("Apakah anda ingin mengistall aplikasi  (y/t)");
//   String jawaban = stdin.readLineSync()!;

//   if (jawaban == "y" || jawaban == "Y") {
//     print("anda akan menginstall aplikasi dart");
//   } else if (jawaban == "t" || jawaban == "T") {
//     print("aborted");
//   }
// }

// nomor 2

// import 'dart:io';

// void main(List<String> args) {
//   print("Masukkan Nama kamu : ");
//   var nama = stdin.readLineSync()!;
//   if (nama != null) {
//     print("Masukkan peran anda : (penyihir, guard, werewolf) ");
//     var peran = stdin.readLineSync()!;
//     if (peran == "penyihir") {
//       print("Selamat datang di dunia werewolf $nama");
//       print("Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf");
//     } else if (peran == "guard") {
//       print("Selamat datang di dunia werewolf $nama");
//       print(
//           "Halo $peran $nama, kamu akan membantu melindungi temanmu dari serangan werewolf");
//     } else if (peran == "werewolf") {
//       print("Selamat datang di dunia werewolf $nama");
//       print("Halo $peran $nama, Kamu akan memakan mangsa setiap malam!");
//     }
//   } else {
//     print("Nama harus diisi!");
//   }
// }

// nomor 3

// import 'dart:io';

// void main(List<String> args) {
//   print("Masukkan nama hari : ");
//   var hari = stdin.readLineSync()!;

//   switch (hari) {
//     case "senin":
//       {
//         print(
//             'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
//         break;
//       }
//     case "selasa":
//       {
//         print(
//             'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
//         break;
//       }
//     case "rabu":
//       {
//         print(
//             'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
//         break;
//       }
//     case "kamis":
//       {
//         print(
//             'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
//         break;
//       }
//     case "jumat":
//       {
//         print('Hidup tak selamanya tentang pacar.');
//         break;
//       }
//     case "sabtu":
//       {
//         print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
//         break;
//       }
//     case "minggu":
//       {
//         print(
//             'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
//         break;
//       }
//   }
// }

// nomor 4

void main() {
  var hari = 21;
  var bulan = 1;
  var tahun = 1945;

  if (hari >= 1 && hari <= 31) {
    if (bulan >= 1 && bulan <= 12) {
      if (tahun >= 1900 && tahun <= 2200) {
        switch (bulan) {
          case 1:
            {
              print(' $hari Januari $tahun');
              break;
            }
          case 2:
            {
              print(' $hari februari $tahun');
              break;
            }
          case 3:
            {
              print(' $hari Maret $tahun');
              break;
            }
          case 4:
            {
              print(' $hari April $tahun');
              break;
            }
          case 5:
            {
              print(' $hari Mei $tahun');
              break;
            }
          case 6:
            {
              print(' $hari Juni $tahun');
              break;
            }
          case 7:
            {
              print(' $hari Juli $tahun');
              break;
            }
          case 8:
            {
              print(' $hari Agustus $tahun');
              break;
            }
          case 9:
            {
              print(' $hari September $tahun');
              break;
            }
          case 10:
            {
              print(' $hari Oktober $tahun');
              break;
            }
          case 11:
            {
              print(' $hari November $tahun');
              break;
            }
          case 12:
            {
              print(' $hari Desember $tahun');
              break;
            }
        }
      } else {
        print("Masukkan Tahun diantara 1900 s.d. 2200");
      }
    } else {
      print("Masukkan Bulan diantara 1 s.d. 12");
    }
  } else {
    print("Masukkan hari diantara tanggal 1 s.d. 31");
  }
}
